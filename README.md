# HereBeDragon

// Software Design M.EEC FEUP
// Main contributers: Ricardo Barbosa Sousa and Armando Sousa
// GPL v3
// In short, free software: Useful, transparent, no warranty

## License
Free software: Useful, transparent, no warranty

## What is it?
A small exercise (quest), solved in Java and C++
Built using VSCode.

This project features:
- The quest: maze(hero, key, exit), movement, game logic, printing, etc
- The project compares a maze with a data structure of a static C matrix and a CPP vector

## Support video(s)
(links will appear here...)

## Tests
Tested in VSC 1.61, windows, g++ (Rev5, Built by MSYS2 project) 10.3.0

## Note
It is advocated to always include "-Wall" in the compiler parameters


## Version
README edited in 2021/10/13

